package com.example.classactivity6;


import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;


public class MainActivity extends AppCompatActivity {

    ListView listView;
    HashMap<Integer,Student> studentHashMap = new HashMap<Integer,Student>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            Void str_result= new GetStudents().execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new GetStudents().execute();
        listView = findViewById(R.id.listView);
        MyAdapter adapter = new MyAdapter(MainActivity.this, studentHashMap);
        listView.setAdapter(adapter);
    }

    private class GetStudents extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this,"Json Data is downloading",Toast.LENGTH_LONG).show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = "https://run.mocky.io/v3/ade12cda-3edb-44bb-b3e1-129166060de2";
            String jsonStr = sh.makeServiceCall(url);

            // Log.e(TAG, "Response from url: " + jsonStr);
            //System.out.println("jsonStr: "+jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray students = jsonObj.getJSONArray("Students");
                    //System.out.println("student: " +students.getJSONObject(0));
                    for (int i = 0 ; i < students.length();i++) {
                        JSONObject studentObj = students.getJSONObject(i);
                        Student tmp = new Student(studentObj.getString("First name"), studentObj.getString("Last name")
                                , studentObj.getString("age"), studentObj.getString("enrollment number"), studentObj.getString("Class"));
                        studentHashMap.put(i, tmp);

                    }







                } catch (final JSONException e) {
                    // Log.e(TAG, "Json parsing error: " + e.getMessage());
                    System.out.println(e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Error: " + e.getMessage());
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                //Log.e(TAG, "Couldn't get json from server.");
                System.out.println("Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }


    }
}