package com.example.classactivity6;



public class Student {
    String fname;
    String lname;
    String age;
    String enrollment_number;
    String classes;


    public Student(String fname, String lname, String age, String enrollment_number, String classes) {
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.enrollment_number = enrollment_number;
        this.classes = classes;
    }
}