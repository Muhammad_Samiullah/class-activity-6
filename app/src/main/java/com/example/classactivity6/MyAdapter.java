package com.example.classactivity6;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

public class MyAdapter  extends BaseAdapter  {
    Context context;
    HashMap<Integer,Student> studentHashMap;


    MyAdapter (Context c, HashMap<Integer,Student> studentHashMap) {
        this.context = c;
        this.studentHashMap = studentHashMap;
        for (int i = 0 ; i <  studentHashMap.size();i++) {
            Student student = studentHashMap.get(i);
            System.out.println("student: " +student.fname + " " + student.lname);
        }

    }

    @Override
    public int getCount() {
        return studentHashMap.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = LayoutInflater.from(context).inflate(R.layout.row, null);
        Student student = studentHashMap.get(position);
        TextView fname = row.findViewById(R.id.fname);
        TextView lname = row.findViewById(R.id.lname);
        TextView age = row.findViewById(R.id.age);
        TextView enrollment_number = row.findViewById(R.id.enrollment_number);
        TextView classes = row.findViewById(R.id.classes);

        fname.setText("First Name: " +student.fname);
        lname.setText("Last Name: "+student.lname);
        age.setText("Age:" + student.age);
        enrollment_number.setText("Enrollment Number: " + student.enrollment_number);
        classes.setText("Class: "+student.classes);
        return row;
    }

}
